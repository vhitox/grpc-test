import com.proto.common.Movie;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class RecommenderServiceImpl extends
        RecommenderServiceGrpc.RecommenderServiceImplBase {
    @Override
    public StreamObserver<Recommender.RecommenderRequest>
    getRecommendedMovie(StreamObserver<Recommender.RecommenderResponse>
                                responseObserver) {
        StreamObserver<Recommender.RecommenderRequest> streamObserver =
                new StreamObserver<Recommender.RecommenderRequest>() {
                    List<Movie> movies = new ArrayList<>();
                    public void onNext(Recommender.RecommenderRequest value) {
                        movies.add(value.getMovie());
                    }
                    public void onError(Throwable t) {
                        responseObserver.onError(Status.INTERNAL
                                .withDescription("Internal server error")
                                .asRuntimeException());
                    }
                    public void onCompleted() {
                        if (movies.size() > 0) {
                            responseObserver
                                    .onNext(Recommender.RecommenderResponse.newBuilder()
                                            .setMovie(findMovieForRecommendation(movies))
                                            .build());
                            responseObserver.onCompleted();
                        } else {
                            responseObserver
                                    .onError(Status.NOT_FOUND
                                            .withDescription("Sorry, found no movies to recommend!").asRuntimeException());
                        }
                    }
                };
        return streamObserver;
    }
    private Movie findMovieForRecommendation(List<Movie> movies) {
        int random = new SecureRandom().nextInt(movies.size());
        return movies.stream().skip(random).findAny().get();
    }
}
