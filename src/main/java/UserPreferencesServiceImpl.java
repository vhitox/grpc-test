import com.proto.common.Movie;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;

import java.security.SecureRandom;

public class UserPreferencesServiceImpl extends
        UserPreferencesServiceGrpc.UserPreferencesServiceImplBase {
    @Override
    public StreamObserver<Userpreferences.UserPreferencesRequest>
    getShortlistedMovies(StreamObserver<Userpreferences.UserPreferencesResponse>
                                 responseObserver) {
        StreamObserver<Userpreferences.UserPreferencesRequest> streamObserver =
                new StreamObserver<Userpreferences.UserPreferencesRequest>() {
                    @Override
                    public void onNext(Userpreferences.UserPreferencesRequest value) {
                        if (isEligible(value.getMovie())) {
                            responseObserver
                                    .onNext(Userpreferences.UserPreferencesResponse
                                            .newBuilder()
                                            .setMovie(value.getMovie()).build());
                        }
                    }
                    @Override
                    public void onError(Throwable t) {
                        responseObserver.onError(Status.INTERNAL
                                .withDescription("Internal server error")
                                .asRuntimeException());
                    }
                    @Override
                    public void onCompleted() {
                        responseObserver.onCompleted();
                    }
                };
        return streamObserver;
    }
    private boolean isEligible(Movie movie) {
        return (new SecureRandom().nextInt() % 4 != 0);
    }
}